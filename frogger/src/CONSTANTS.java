public class CONSTANTS {
    public static int FROG_SIZE = 50;
    final static int WINDOW_WIDTH = 500;
    final static int TIMER_BAR_WIDTH = 400;
    final static int WINDOW_HEIGHT = 750;
    final static int HEIGHT_DIVISION = WINDOW_HEIGHT / FROG_SIZE;
    final static int ROAD_START_HEIGHT = 300;
    final static int RIVER_START_HEIGHT = 0;

    public static int FROG_SPEED = 50;
    public static int FROG_START_X = 200;
    public static int FROG_START_Y = WINDOW_HEIGHT - FROG_SIZE * 2;

    final static int TEXTBOX_WIDTH = 100;
    final static int TEXTBOX_HEIGHT = 50;
    public static int LOG_SIZE = 50;
    final static int CAR_SIZE = 50;

    final static int FROG_RADIUS_COMPARE = CAR_SIZE * 2;
}
