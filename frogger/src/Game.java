import javax.swing.JFrame;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;

public class Game extends JFrame implements KeyListener {
    Board board;
    Timer gameTimer;
    Timer timer;
    int timeLeft;

    CollisionHandler collisionHandler;

    public void run() {
        setup();
    }

    public void setup() {
        timeLeft = 60;
        collisionHandler = new CollisionHandler();
        gameTimer = new Timer();
        timer = new Timer();
        board = new Board();
        this.add(board);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(board);
        this.pack();
        this.setVisible(true);
        this.addKeyListener(this);
        TimerTask gameloop = new TimerTask() {
            public void run() {
                gameloop();
            }
        };
        gameTimer.scheduleAtFixedRate(gameloop, 0, 20);
        TimerTask timerBarTask = new TimerTask() {
            public void run() {
                timerBar();
            }
        };
        timer.scheduleAtFixedRate(timerBarTask, 0, 1000);
    }

    public void checkLoss() {
        if (this.board.respawnFrog(0)) {
            this.gameTimer.cancel();
            this.timer.cancel();
            System.out.println("You lose");
        }
    }

    public void gameloop() {
        if (timeLeft <= 0) {
            this.gameTimer.cancel();
            this.timer.cancel();
            System.out.println("You lose");
        }
        if (collisionHandler.checkCollision(this.board.getRoads().getRoads(), this.board.getFrog())) {
            checkLoss();
            ;
        } else if (this.board.getFrog().getY() < 0) {
            this.board.respawnFrog(1);
            System.out.println("You win!");
        }
        int water = collisionHandler.checkWater(this.board.getRivers().getRivers(), this.board.getFrog());
        if (water == 0 || water == 1) {
            this.board.moveFrogWithLog(water);
        } else if (water == 2) {
            this.board.respawnFrog(0);
        }

        this.board.repaint();
    }

    public void timerBar() {
        this.timeLeft--;
        this.board.timeLeft(timeLeft);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        this.board.moveFrog(e.getKeyCode());
    }
}
