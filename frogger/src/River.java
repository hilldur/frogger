import java.util.ArrayList;

import java.awt.Graphics;

public class River {
    ArrayList<Log> logs = new ArrayList<Log>();
    int y1;
    int height;
    int y2;
    int direction;

    public River(int y1, int height, int direction) {
        this.y1 = y1;
        this.height = height;
        this.y2 = y1 + height;
        this.direction = direction;
        createLogs();
    }

    public void draw(Graphics g) {
        logs.forEach((log) -> {
            log.draw(g);
        });
    }

    public void createLogs() {
        double amountOfThrees = Math.random() * 2 + 1;

        for (int i = 0; i < amountOfThrees; i++) {
            double offset = Math.random() * 350;
            int x;
            if (direction >= 0.5) {
                x = CONSTANTS.WINDOW_WIDTH + (CONSTANTS.LOG_SIZE) + (int) offset;
                for (int y = 0; y < 3; y++) {
                    logs.add(new Log(x - (CONSTANTS.CAR_SIZE * y), y1, CONSTANTS.LOG_SIZE, direction));
                }
            } else {
                x = 0 - (CONSTANTS.LOG_SIZE) - (int) offset;
                for (int y = 0; y < 3; y++) {
                    logs.add(new Log(x + (CONSTANTS.CAR_SIZE * y), y1, CONSTANTS.LOG_SIZE, direction));
                }
            }

        }
    }

    public ArrayList<Log> getLogList() {
        return logs;
    }

    public int getY() {
        return y1;
    }

    public int getDirection() {
        return direction;
    }
}
