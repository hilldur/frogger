import java.awt.Color;
import java.awt.Graphics;

public class Car {
    int y1;
    int size;
    int direction;
    int x = 0;
    Color color;

    public Car(int x, int y1, int size, int direction) {
        this.y1 = y1;
        this.size = size;
        this.direction = direction;
        this.x = x;

        double colorInt = Math.random() * 4;
        if (colorInt > 0 && colorInt < 1) {
            color = Color.RED;
        } else if (colorInt > 1 && colorInt < 2) {
            color = Color.YELLOW;
        } else {
            color = Color.GREEN;
        }
    }

    public void draw(Graphics g) {
        g.setColor(color);
        if (direction == 0) {
            g.fillRect(x, y1, size, size);
            g.setColor(Color.BLACK);
            g.fillRect(x, y1, 10, 10);
            g.fillRect(x + 30, y1, 20, 10);
            g.fillRect(x + 40, 20, 5, 10);
            g.fillRect(x, y1 + 20, 10, 20);
            g.setColor(Color.WHITE);
            g.fillRect(x + size / 2, y1 + size / 3, size / 3, size - size / 3);
            x++;
            if (x >= CONSTANTS.WINDOW_WIDTH + CONSTANTS.CAR_SIZE) {
                x = 0 - CONSTANTS.CAR_SIZE;
            }
        } else {
            g.fillRect(x, y1, size, size);
            g.setColor(Color.BLACK);
            g.fillRect(x, y1, 10, 10);
            g.fillRect(x + 30, y1, 20, 10);
            g.fillRect(x + 40, y1 + 20, 10, 20);
            g.setColor(Color.WHITE);
            g.fillRect(x + size / 4, y1 + size / 4, size / 3, size - size / 4);
            x--;
            if (x <= 0 - CONSTANTS.CAR_SIZE) {
                x = CONSTANTS.WINDOW_WIDTH + CONSTANTS.CAR_SIZE;
            }
        }
    }

    public int getY() {
        return y1;
    }

    public int getX() {
        return x;
    }
}
