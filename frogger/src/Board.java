import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Font;

public class Board extends JPanel {
    Frog frog;
    Roads roads;
    JLabel lifeLabel;
    JLabel winsLabel;
    int lives;
    int wins;
    Rivers rivers;
    double timeLeft = 60;
    // int width = CONSTANTS.TIMER_BAR_WIDTH * (timeLeft / 60);

    public Board() {
        this.frog = new Frog();
        this.roads = new Roads();
        this.rivers = new Rivers();
        this.lives = 3;
        lifeLabel = new JLabel("Lives: " + lives);
        lifeLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
        lifeLabel.setBounds(CONSTANTS.WINDOW_WIDTH / 2, 0, CONSTANTS.TEXTBOX_WIDTH, CONSTANTS.TEXTBOX_HEIGHT);
        this.add(lifeLabel);

        winsLabel = new JLabel("Wins: " + wins);
        winsLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
        winsLabel.setBounds(CONSTANTS.WINDOW_WIDTH / 2, 0, CONSTANTS.TEXTBOX_WIDTH, CONSTANTS.TEXTBOX_HEIGHT);
        this.add(winsLabel);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(CONSTANTS.WINDOW_WIDTH, CONSTANTS.WINDOW_HEIGHT);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.GRAY);
        this.roads.draw(g);
        this.rivers.draw(g);
        this.frog.draw(g);
        double percent = timeLeft / 60;
        int width = (int) (CONSTANTS.TIMER_BAR_WIDTH * percent);
        g.setColor(Color.RED);
        g.fillRect(50, CONSTANTS.WINDOW_HEIGHT - 30, CONSTANTS.TIMER_BAR_WIDTH, 20);
        g.setColor(Color.GREEN);
        g.fillRect(50, CONSTANTS.WINDOW_HEIGHT - 30, width, 20);
    }

    public void moveFrog(int charCode) {
        this.frog.move(charCode);
    };

    public Frog getFrog() {
        return frog;
    }

    public void timeLeft(int time) {
        this.timeLeft = time;
    }

    public Roads getRoads() {
        return roads;
    }

    public Rivers getRivers() {
        return rivers;
    }

    public boolean respawnFrog(int n) {

        this.frog.respawn();
        if (n == 0) {
            this.lives--;
            if (lives <= 0) {
                return true;
            }
        } else {
            this.wins++;
        }

        lifeLabel.setText("Lives: " + lives);
        winsLabel.setText("Wins: " + wins);
        return false;
    }

    public void moveFrogWithLog(int direction) {
        this.frog.moveWithLog(direction);
    }
}
