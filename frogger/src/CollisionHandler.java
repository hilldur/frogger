import java.util.ArrayList;

public class CollisionHandler {
    public boolean checkCollision(ArrayList<Road> roads, Frog frog) {
        for (int i = 0; i < roads.size(); i++) {
            ArrayList<Car> cars = roads.get(i).getCarList();
            for (int y = 0; y < cars.size(); y++) {
                if ((frog.getY() == cars.get(y).getY())) {
                    if ((frog.getX() + CONSTANTS.FROG_SIZE >= cars.get(y).getX())
                            && (frog.getX() < cars.get(y).getX() + CONSTANTS.CAR_SIZE)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public int checkWater(ArrayList<River> rivers, Frog frog) {
        for (int i = 0; i < rivers.size(); i++) {
            if ((frog.getY() == rivers.get(i).getY())) {
                ArrayList<Log> logs = rivers.get(i).getLogList();
                for (int y = 0; y < logs.size(); y++) {
                    if ((frog.getX() + CONSTANTS.FROG_SIZE >= logs.get(y).getX())
                            && (frog.getX() < logs.get(y).getX() + CONSTANTS.CAR_SIZE)) {
                        return rivers.get(i).getDirection();
                    }
                }
                return 2;
            }
        }
        return 3;
    }
}
