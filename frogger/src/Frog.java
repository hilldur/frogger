
import java.awt.Color;
import java.awt.Graphics;

public class Frog {
    int x = CONSTANTS.FROG_START_X;
    int y = CONSTANTS.FROG_START_Y;
    int direction = 38;

    public void draw(Graphics g) {
        switch (direction) {
            // Left
            case 37:
                g.setColor(Color.GREEN);
                g.fillRect(x, y, 50, 50);
                g.setColor(Color.BLACK);
                g.fillRect(x + 10, y, 10, 10);
                g.fillRect(x + 10, y + 40, 10, 10);
                g.setColor(Color.RED);
                g.fillRect(x, y + 10, 10, 30);
                break;
            // Up
            case 38:
                g.setColor(Color.GREEN);
                g.fillRect(x, y, 50, 50);
                g.setColor(Color.BLACK);
                g.fillRect(x, y + 10, 10, 10);
                g.fillRect(x + 40, y + 10, 10, 10);
                g.setColor(Color.RED);
                g.fillRect(x + 10, y, 30, 10);
                break;
            // Right
            case 39:
                g.setColor(Color.GREEN);
                g.fillRect(x, y, 50, 50);
                g.setColor(Color.BLACK);
                g.fillRect(x + 30, y, 10, 10);
                g.fillRect(x + 30, y + 40, 10, 10);
                g.setColor(Color.RED);
                g.fillRect(x + 40, y + 10, 10, 30);
                break;
            // Down
            case 40:
                g.setColor(Color.GREEN);
                g.fillRect(x, y, 50, 50);
                g.setColor(Color.BLACK);
                g.fillRect(x + 40, y + 30, 10, 10);
                g.fillRect(x, y + 30, 10, 10);
                g.setColor(Color.RED);
                g.fillRect(x + 10, y + 40, 30, 10);
                break;

        }

    }

    public void move(int charCode) {
        if (charCode >= 37 && charCode <= 40)
            this.direction = charCode;
        switch (charCode) {
            case 39:
                if (x < CONSTANTS.WINDOW_WIDTH - CONSTANTS.FROG_SIZE) {
                    x += CONSTANTS.FROG_SPEED;
                }
                break;
            case 38:
                y -= CONSTANTS.FROG_SPEED;
                break;
            case 40:
                if (y < CONSTANTS.WINDOW_HEIGHT - CONSTANTS.FROG_SIZE) {
                    y += CONSTANTS.FROG_SPEED;
                }
                break;
            case 37:
                if (x > 0) {
                    x -= CONSTANTS.FROG_SPEED;
                }
                break;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void respawn() {
        this.y = CONSTANTS.FROG_START_Y;
        this.x = CONSTANTS.FROG_START_X;
    }

    public void moveWithLog(int direction) {
        if (direction == 0) {
            this.x++;
        } else {
            this.x--;
        }
    }
}
