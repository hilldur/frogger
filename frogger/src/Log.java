
import java.awt.Color;
import java.awt.Graphics;

public class Log {
    int y1;
    int size;
    int direction;
    int x = 0;
    Color logColor = new Color(145, 103, 83);
    Color logDetailColor = new Color(130, 86, 83);

    public Log(int x, int y1, int size, int direction) {
        this.y1 = y1;
        this.size = size;
        this.direction = direction;
        this.x = x;
    }

    public void draw(Graphics g) {
        g.setColor(logColor);
        if (direction == 0) {
            g.fillRect(x, y1, size, size);
            x++;
            if (x >= CONSTANTS.WINDOW_WIDTH + CONSTANTS.CAR_SIZE) {
                x = 0 - CONSTANTS.CAR_SIZE;
            }
        } else {
            g.fillRect(x, y1, size, size);
            x--;
            if (x <= 0 - CONSTANTS.CAR_SIZE) {
                x = CONSTANTS.WINDOW_WIDTH + CONSTANTS.CAR_SIZE;
            }
        }

        g.setColor(logDetailColor);
        g.fillRect(x, y1 - size / 8, size, size / 8);
        g.fillRect(x, y1 + size - size / 8, size, size / 8);
        g.fillRect(x - size / 8, y1, size / 8, size);
        g.fillRect(x + size - size / 8, y1, size / 8, size);
    }

    public int getY() {
        return y1;
    }

    public int getX() {
        return x;
    }
}
