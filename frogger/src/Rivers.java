
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Rivers {
    boolean firstDraw = true;
    ArrayList<River> rivers = new ArrayList<River>();

    public void draw(Graphics g) {
        if (!firstDraw) {
            rivers.forEach((river) -> {
                g.setColor(new Color(55, 124, 165));
                g.fillRect(0, river.y1, CONSTANTS.WINDOW_WIDTH, river.height);
                river.draw(g);
            });
            g.setColor(new Color(26, 172, 45));
            g.fillRect(0, 0, CONSTANTS.WINDOW_WIDTH, CONSTANTS.CAR_SIZE);
        } else {
            g.setColor(Color.BLUE);
            for (int i = 0; i < 4; i++) {
                int y1 = 50 + (CONSTANTS.WINDOW_HEIGHT / CONSTANTS.HEIGHT_DIVISION) * i;
                int height = CONSTANTS.CAR_SIZE;
                g.fillRect(0, y1, CONSTANTS.WINDOW_WIDTH,
                        height);
                if (i % 2 == 0) {
                    rivers.add(new River(y1, height, 0));
                } else {
                    rivers.add(new River(y1, height, 1));
                }

            }
            g.setColor(new Color(26, 172, 45));
            g.fillRect(0, 0, CONSTANTS.WINDOW_WIDTH, CONSTANTS.CAR_SIZE);
            firstDraw = false;
        }
    }

    public ArrayList<River> getRivers() {
        return rivers;
    }
}
