import java.util.ArrayList;
import java.awt.Graphics;

public class Road {
    ArrayList<Car> cars = new ArrayList<Car>();
    int y1;
    int height;
    int y2;

    public Road(int y1, int height) {
        this.y1 = y1;
        this.height = height;
        this.y2 = y1 + height;

        createCars();
    }

    public void draw(Graphics g) {
        cars.forEach((car) -> {
            car.draw(g);
        });
    }

    public void createCars() {
        double amount = Math.random() * 5 + 1;
        double direction = Math.random();
        if (direction >= 0.5) {
            direction = 1;
        } else {
            direction = 0;
        }

        for (int i = 0; i < amount; i++) {
            int x;
            double space = Math.random() * 3 + 1;
            if (direction >= 0.5) {
                x = CONSTANTS.WINDOW_WIDTH + (i * CONSTANTS.CAR_SIZE * (int) space);
            } else {
                x = 0 - (i * CONSTANTS.CAR_SIZE * (int) space);
            }
            cars.add(new Car(x, y1, CONSTANTS.CAR_SIZE, (int) direction));
        }
    }

    public ArrayList<Car> getCarList() {
        return cars;
    }
}
