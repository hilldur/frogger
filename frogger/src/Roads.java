
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Roads {
    boolean firstDraw = true;
    ArrayList<Road> roads = new ArrayList<Road>();

    public void draw(Graphics g) {
        if (!firstDraw) {
            roads.forEach((road) -> {
                g.setColor(Color.BLACK);
                g.fillRect(0, road.y1, CONSTANTS.WINDOW_WIDTH, road.height);
                road.draw(g);
            });
            g.setColor(new Color(26, 172, 45));
            g.fillRect(0, 0, CONSTANTS.WINDOW_WIDTH, CONSTANTS.CAR_SIZE);
        } else {
            g.setColor(Color.BLACK);
            for (int i = 0; i <= 6; i++) {
                if (i % 2 == 0) {
                    int y1 = CONSTANTS.ROAD_START_HEIGHT + (CONSTANTS.WINDOW_HEIGHT / CONSTANTS.HEIGHT_DIVISION) * i;
                    int height = CONSTANTS.CAR_SIZE;
                    g.fillRect(0, y1, CONSTANTS.WINDOW_WIDTH,
                            height);
                    roads.add(new Road(y1, height));
                }
            }
            g.setColor(new Color(26, 172, 45));
            g.fillRect(0, 0, CONSTANTS.WINDOW_WIDTH, CONSTANTS.CAR_SIZE);
            firstDraw = false;
        }
    }

    public ArrayList<Road> getRoads() {
        return roads;
    }
}
